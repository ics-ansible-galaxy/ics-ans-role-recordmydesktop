ics-ans-role-recordmydesktop
============================

**DEPECRATED**

This role was only installing some yum packages. They were moved to the ics-ans-role-desktop-base role.

Ansible role to install [recordMyDesktop](http://recordmydesktop.sourceforge.net/about.php),
a desktop session recorder for Linux.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

None

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-recordmydesktop
```

License
-------

BSD 2-clause
